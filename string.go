package typo

import (
	"fmt"
	"reflect"
	"strconv"

	"golang.org/x/exp/constraints"
)

func CommonPrefix(strings ...string) string {
	if len(strings) == 0 {
		return ""
	}

	prefix := []rune(strings[0])

	for _, str := range strings[1:] {
		var (
			i     = 0
			runes = []rune(str)
		)

		for ; i < len(prefix) && i < len(runes); i++ {
			if prefix[i] != runes[i] {
				break
			}
		}

		prefix = prefix[:i]
	}

	return string(prefix)
}

func ParseComplex[T constraints.Complex](str string) (t T, err error) {
	return parseT[T](
		strconv.ParseComplex,
		func(v *reflect.Value, c complex128) {
			v.SetComplex(c)
		},
		str,
	)
}

func ParseFloat[T constraints.Float](str string) (t T, err error) {
	return parseT[T](strconv.ParseFloat, func(v *reflect.Value, f float64) {
		v.SetFloat(f)
	}, str)
}

func ParseInt[T constraints.Signed](str string) (t T, err error) {
	return parseT[T](func(str string, bits int) (int64, error) {
		return strconv.ParseInt(str, 0, bits)
	}, func(v *reflect.Value, i int64) {
		v.SetInt(i)
	}, str)
}

func ParseUint[T constraints.Unsigned](str string) (t T, err error) {
	return parseT[T](func(str string, bits int) (uint64, error) {
		return strconv.ParseUint(str, 0, bits)
	}, func(v *reflect.Value, u uint64) {
		v.SetUint(u)
	}, str)
}

func SplitPairString(input string, delimiter rune) (first, second string) {
	f, s := SplitPair([]rune(input), delimiter)
	return string(f), string(s)
}

func parseT[Out any, Tmp any](
	p func(string, int) (Tmp, error),
	s func(*reflect.Value, Tmp),
	str string,
) (t Out, err error) {
	var (
		v   = reflect.ValueOf(&t).Elem()
		tmp Tmp
	)
	tmp, err = p(str, v.Type().Bits())

	if err != nil {
		err = fmt.Errorf("%w: bad %T value %q", ErrInvalidValue, t, str)
		return
	}

	s(&v, tmp)

	return
}

type Number interface {
	constraints.Complex | constraints.Float | constraints.Integer
}
