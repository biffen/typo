package pointer

func Deref[T any](t *T) T {
	return *t
}

func IfNil[T any](t *T, otherwise T) T {
	if t == nil {
		return otherwise
	}

	return *t
}

func To[T any](t T) *T {
	return &t
}
