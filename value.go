package typo

import "reflect"

func Default[T any](t T, def T) T {
	if reflect.ValueOf(t).IsZero() {
		return def
	}

	return t
}
