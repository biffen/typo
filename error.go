package typo

import "fmt"

const (
	ErrBadTarget    Error = "bad target"
	ErrInvalidValue Error = "invalid value"
)

var _ error = (*Error)(nil)

func RecoverError(f func()) (err error) {
	defer func() {
		if r := recover(); r != nil {
			switch t := r.(type) {
			case error:
				err = t
			default:
				err = fmt.Errorf("%v", t)
			}
		}
	}()

	f()

	return
}

type Error string

func (err Error) Error() string {
	return string(err)
}
