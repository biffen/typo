package typo

import (
	"context"
	"encoding"
	"flag"
	"fmt"
	"net"
	"net/netip"
	"net/url"
	"reflect"
	"strconv"
	"strings"
	"time"

	errör "gitlab.com/biffen/error"
)

const (
	DefaultListSeparator        = ','
	DefaultMapKeyValueSeparator = '='
	DefaultMapSeparator         = ','
)

func FromString(ctx context.Context, str string, dest any) error {
	return new(StringConverter).
		Convert(ctx, str, dest)
}

func wrapParse[T any](
	t *T,
	parser func(string) (T, error),
) func(context.Context, string) error {
	return func(_ context.Context, str string) error {
		tmp, err := parser(str)
		if err != nil {
			return err
		}
		*t = tmp
		return nil
	}
}

type StringConverter struct {
	ListSeparator        rune
	MapKeyValueSeparator rune
	MapSeparator         rune
	SliceAppend          bool
}

func (s *StringConverter) Convert(
	ctx context.Context,
	str string,
	dest any,
) error {
	f, err := s.ConvertFunc(dest)
	if err != nil {
		return err
	}

	return f(ctx, str)
}

func (s *StringConverter) ConvertFunc(
	dest any,
) (func(context.Context, string) error, error) {
	switch t := dest.(type) {
	case encoding.TextUnmarshaler:
		return func(_ context.Context, str string) error {
			return t.UnmarshalText([]byte(str))
		}, nil

	case flag.Value:
		return func(_ context.Context, str string) error {
			return t.Set(str)
		}, nil
	}

	t := reflect.TypeOf(dest)

	if t.Kind() != reflect.Func {
		if f, err := func() (f func(context.Context, string) error, err error) {
			defer func() {
				if r := recover(); r != nil {
					switch t := r.(type) {
					case error:
						err = t
					default:
						err = fmt.Errorf("%v", t)
					}
					err = errör.Composite(ErrBadTarget, err)
				}
			}()

			switch t.Elem().Kind() {
			case reflect.Array:
				return s.convertList(dest, t)

			case reflect.Map:
				return s.convertMap(dest, t)

			case reflect.Pointer:
				return s.convertPointer(dest, t)

			case reflect.Slice:
				if s.SliceAppend {
					return s.convertSliceAppend(dest, t)
				}
				return s.convertList(dest, t)

			default:
				return nil, nil
			}
		}(); err != nil {
			return nil, errör.Composite(ErrBadTarget, err)
		} else if f != nil {
			return f, nil
		}
	}

	switch t := dest.(type) {
	case *net.IP:
		return func(_ context.Context, str string) error {
			tmp := net.ParseIP(str)
			if tmp == nil {
				return fmt.Errorf("failed to parse IP address from %q", str)
			}
			*t = tmp
			return nil
		}, nil

	case *netip.Addr:
		return wrapParse(t, netip.ParseAddr), nil

	case *netip.AddrPort:
		return wrapParse(t, netip.ParseAddrPort), nil

	case *netip.Prefix:
		return wrapParse(t, netip.ParsePrefix), nil

	case *time.Duration:
		return wrapParse(t, time.ParseDuration), nil

	case *url.URL:
		return func(_ context.Context, str string) error {
			tmp, err := url.Parse(str)
			if err != nil {
				return err
			}
			*t = *tmp
			return nil
		}, nil

	case func(string):
		return func(_ context.Context, str string) error {
			t(str)
			return nil
		}, nil

	case func(string) error:
		return func(_ context.Context, str string) error {
			return t(str)
		}, nil

	case func(context.Context, string):
		return func(ctx context.Context, str string) error {
			t(ctx, str)
			return nil
		}, nil

	case func(context.Context, string) error:
		return func(ctx context.Context, str string) error {
			return t(ctx, str)
		}, nil

	case *bool:
		return wrapParse(t, strconv.ParseBool), nil

	case *float32:
		return wrapParse(t, ParseFloat[float32]), nil
	case *float64:
		return wrapParse(t, ParseFloat[float64]), nil
	case *int:
		return wrapParse(t, ParseInt[int]), nil
	case *int8:
		return wrapParse(t, ParseInt[int8]), nil
	case *int16:
		return wrapParse(t, ParseInt[int16]), nil
	case *int32: // Includes *rune.
		return wrapParse(t, ParseInt[int32]), nil
	case *int64:
		return wrapParse(t, ParseInt[int64]), nil

	case *string:
		return func(_ context.Context, str string) error {
			*t = str
			return nil
		}, nil

	case *uint:
		return wrapParse(t, ParseUint[uint]), nil
	case *uint8: // Includes *byte.
		return wrapParse(t, ParseUint[uint8]), nil
	case *uint16:
		return wrapParse(t, ParseUint[uint16]), nil
	case *uint32:
		return wrapParse(t, ParseUint[uint32]), nil
	case *uint64:
		return wrapParse(t, ParseUint[uint64]), nil

	default:
		return nil, fmt.Errorf("can’t parse to %T", dest)
	}
}

func (s *StringConverter) convertList(
	dest any,
	t reflect.Type,
) (func(context.Context, string) error, error) {
	var (
		element               = reflect.New(t.Elem().Elem())
		elementConverter, err = s.ConvertFunc(element.Interface())
	)
	if err != nil {
		return nil, fmt.Errorf(
			"can‘t parse to element of %s: %w",
			t.Elem(),
			err,
		)
	}

	return func(ctx context.Context, str string) error {
		var (
			tmp  reflect.Value
			strs = strings.Split(
				str,
				string(Default(s.ListSeparator, DefaultListSeparator)),
			)
		)

		if t.Elem().Kind() == reflect.Array {
			tmp = reflect.ValueOf(dest).Elem()
			if tmp.Len() < len(strs) {
				return fmt.Errorf(
					"too many values for array (%d > %d)",
					len(strs),
					tmp.Len(),
				)
			}
		} else {
			tmp = reflect.MakeSlice(t.Elem(), len(strs), len(strs))
		}

		for i := 0; i < len(str) && i < tmp.Len(); i++ {
			if err := elementConverter(ctx, strs[i]); err != nil {
				return err
			}
			tmp.Index(i).Set(element.Elem())
		}

		reflect.ValueOf(dest).Elem().Set(tmp)

		return nil
	}, nil
}

func (s *StringConverter) convertMap(
	dest any,
	t reflect.Type,
) (func(context.Context, string) error, error) {
	return func(ctx context.Context, str string) error {
		tmp := reflect.MakeMap(t.Elem())

		for str != "" {
			var (
				key      string
				tmpKey   = reflect.New(t.Elem().Key())
				tmpValue = reflect.New(t.Elem().Elem())
			)

			key, str = SplitPairString(
				str,
				Default(
					s.MapKeyValueSeparator,
					DefaultMapKeyValueSeparator,
				),
			)

			if err := s.Convert(ctx, key, tmpKey.Interface()); err != nil {
				return err
			}

			if str != "" {
				var val string
				val, str = SplitPairString(
					str,
					Default(s.MapSeparator, DefaultMapSeparator),
				)

				if err := s.Convert(ctx, val, tmpValue.Interface()); err != nil {
					return err
				}
			}

			// TODO check clash

			tmp.SetMapIndex(tmpKey.Elem(), tmpValue.Elem())
		}

		reflect.ValueOf(dest).Elem().Set(tmp)

		return nil
	}, nil
}

func (s *StringConverter) convertPointer(
	dest any,
	t reflect.Type,
) (func(context.Context, string) error, error) {
	return func(ctx context.Context, str string) error {
		tmp := reflect.New(t.Elem().Elem())

		if err := s.Convert(ctx, str, tmp.Interface()); err != nil {
			return err
		}

		reflect.ValueOf(dest).Elem().Set(tmp)

		return nil
	}, nil
}

func (s *StringConverter) convertSliceAppend(
	dest any,
	t reflect.Type,
) (func(context.Context, string) error, error) {
	var (
		element               = reflect.New(t.Elem().Elem())
		elementConverter, err = s.ConvertFunc(element.Interface())
	)
	if err != nil {
		return nil, fmt.Errorf(
			"can‘t parse to element of %s: %w",
			t.Elem(),
			err,
		)
	}

	return func(ctx context.Context, str string) error {
		if err := elementConverter(ctx, str); err != nil {
			return err
		}

		slice := reflect.ValueOf(dest).Elem()
		slice.Set(reflect.Append(slice, element.Elem()))

		return nil
	}, nil
}
