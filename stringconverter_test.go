package typo_test

import (
	"context"
	"encoding"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/biffen/typo"
	"gitlab.com/biffen/typo/pointer"
)

var _ encoding.TextUnmarshaler = (*testTextUnmarshaler)(nil)

func TestFromString(t *testing.T) {
	t.Parallel()

	t.Run("const", func(t *testing.T) {
		const (
			c = "const"
		)
		err := typo.FromString(context.Background(), "", c)
		assert.Error(t, err)
	})

	t.Run("func", func(t *testing.T) {
		var tmp string
		err := typo.FromString(context.Background(), "foo", func(str string) {
			tmp = str
		})
		assert.NoError(t, err)
		assert.Equal(t, "foo", tmp)
	})

	testFromString(t, "", "", false)
	testFromString(t, "1=1.2,42=1.7", map[int]float32{1: 1.2, 42: 1.7}, false)
	testFromString(t, "1h42m", time.Hour+42*time.Minute, false)
	testFromString(t, "42", 42, false)
	testFromString(t, "42", pointer.To(pointer.To(42)), false)
	testFromString(t, "42,17", [2]int{42, 17}, false)
	testFromString(t, "42,17", []int{42, 17}, false)
	testFromString(t, "foo", "foo", false)
	testFromString(t, "foo", testTextUnmarshaler("*foo*"), false)

	testFromString(t, "", 0, true)
	testFromString(t, "", time.Duration(0), true)
	testFromString(t, "42,17,99", [2]int{}, true)
	testFromString(t, "foo", 0, true)
}

func TestStringConverter_Convert(t *testing.T) {
	t.Parallel()

	ctx := context.Background()

	t.Run("map other separators", func(t *testing.T) {
		t.Parallel()

		var (
			m  map[string]string
			sc typo.StringConverter
		)
		sc.MapKeyValueSeparator = ':'
		sc.MapSeparator = ';'

		err := sc.Convert(ctx, "foo:bar;bar:baz;baz:;:foo", &m)
		assert.NoError(t, err)
		assert.Equal(t, map[string]string{
			"":    "foo",
			"bar": "baz",
			"baz": "",
			"foo": "bar",
		}, m)
	})

	t.Run("map same separator", func(t *testing.T) {
		t.Parallel()

		var (
			m  map[string]string
			sc typo.StringConverter
		)
		sc.MapKeyValueSeparator = ' '
		sc.MapSeparator = ' '

		err := sc.Convert(ctx, "foo bar bar baz", &m)
		assert.NoError(t, err)
		assert.Equal(t, map[string]string{
			"bar": "baz",
			"foo": "bar",
		}, m)
	})
}

func testFromString[T any](t *testing.T, str string, expected T, error bool) {
	t.Helper()

	t.Run(fmt.Sprintf("%q→%T", str, expected), func(t *testing.T) {
		t.Helper()
		t.Parallel()

		var tmp T
		err := typo.FromString(context.Background(), str, &tmp)

		if error {
			assert.Error(t, err)
			return
		}

		assert.NoError(t, err)
		assert.Equal(t, expected, tmp)
	})
}

type testTextUnmarshaler string

func (t *testTextUnmarshaler) UnmarshalText(text []byte) error {
	*t = testTextUnmarshaler("*" + string(text) + "*")
	return nil
}
