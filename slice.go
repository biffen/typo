package typo

func SplitPair[T comparable](input []T, delimiter T) (first, second []T) {
	for i, t := range input {
		if t == delimiter {
			return input[:i], input[i+1:]
		}
	}

	return input, []T{}
}
