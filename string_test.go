package typo_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/biffen/typo"
)

func TestCommonPrefix(t *testing.T) {
	t.Parallel()

	for _, c := range [...]struct {
		Strings  []string
		Expected string
	}{
		{},

		{
			Strings:  []string{""},
			Expected: "",
		},

		{
			Strings:  []string{"", ""},
			Expected: "",
		},

		{
			Strings:  []string{"foo"},
			Expected: "foo",
		},

		{
			Strings:  []string{"", "foo"},
			Expected: "",
		},

		{
			Strings:  []string{"foobar", "foo"},
			Expected: "foo",
		},

		{
			Strings:  []string{"foo", "foobar"},
			Expected: "foo",
		},

		{
			Strings:  []string{"foo", "foobar", "foobaz"},
			Expected: "foo",
		},

		{
			Strings:  []string{"foo", "foobar", "baz"},
			Expected: "",
		},
	} {
		c := c

		t.Run(fmt.Sprintf("%q", c.Strings), func(t *testing.T) {
			t.Parallel()

			actual := typo.CommonPrefix(c.Strings...)
			assert.Equal(t, c.Expected, actual)
		})
	}
}

func TestParseFloat(t *testing.T) {
	t.Parallel()

	f32, err := typo.ParseFloat[float32]("0")
	assert.NoError(t, err)
	assert.InDelta(t, float32(0), f32, 0.01)

	f64, err := typo.ParseFloat[float64]("1.2")
	assert.NoError(t, err)
	assert.InDelta(t, float64(1.2), f64, 0.01)

	_, err = typo.ParseFloat[float32]("nope")
	assert.ErrorIs(t, err, typo.ErrInvalidValue)
}

func TestParseInt(t *testing.T) {
	t.Parallel()

	i, err := typo.ParseInt[int]("42")
	assert.NoError(t, err)
	assert.EqualValues(t, 42, i)

	i8, err := typo.ParseInt[int8]("42")
	assert.NoError(t, err)
	assert.EqualValues(t, 42, i8)

	i8, err = typo.ParseInt[int8]("128")
	assert.ErrorIs(t, err, typo.ErrInvalidValue, "too large")

	i64, err := typo.ParseInt[int64]("2147483648")
	assert.NoError(t, err)
	assert.EqualValues(t, 2147483648, i64)

	_, err = typo.ParseInt[int]("nope")
	assert.ErrorIs(t, err, typo.ErrInvalidValue)
}

func TestParseUint(t *testing.T) {
	t.Parallel()

	u, err := typo.ParseUint[uint]("42")
	assert.NoError(t, err)
	assert.EqualValues(t, 42, u)

	u8, err := typo.ParseUint[uint8]("42")
	assert.NoError(t, err)
	assert.EqualValues(t, 42, u8)

	_, err = typo.ParseUint[uint8]("256")
	assert.ErrorIs(t, err, typo.ErrInvalidValue, "too large")

	u64, err := typo.ParseUint[uint64]("4294967296")
	assert.NoError(t, err)
	assert.EqualValues(t, 4294967296, u64)

	_, err = typo.ParseUint[uint]("nope")
	assert.ErrorIs(t, err, typo.ErrInvalidValue)
}

func TestSplitPairString(t *testing.T) {
	t.Parallel()

	for _, c := range [...]struct {
		String        string
		Sep           rune
		ExpectedKey   string
		ExpectedValue string
	}{
		{"foo", '=', "foo", ""},
		{"foo=", '=', "foo", ""},
		{"foo=bar", '=', "foo", "bar"},
		{"foo=bar=baz", '=', "foo", "bar=baz"},
	} {
		t.Run(fmt.Sprintf("%q, %q", c.String, c.Sep), func(t *testing.T) {
			t.Parallel()

			key, value := typo.SplitPairString(c.String, c.Sep)
			assert.Equal(t, c.ExpectedKey, key)
			assert.Equal(t, c.ExpectedValue, value)
		})
	}
}
